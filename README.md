# Les Tours de Hanoï

## Developpeur

- Ringot Arthur


## <br/> Algorithme utilisé : L'algorithme itératif :
 Il peut être retrouvé rapidement sur le net :

 Par exemple :
 - [la page wikipedia](https://fr.wikipedia.org/wiki/Tours_de_Hano%C3%AF#Solution_it%C3%A9rative)
 - [un blog explicatif](http://blog.marcinchwedczuk.pl/iterative-solution-to-towers-of-hanoi-problem)
 - [un pdf explicatif](https://maartenfokkinga.github.io/utwente/mmf2000a.pdf)
 - [une page de forum](https://openclassrooms.com/forum/sujet/algorithmique-hanoi-en-iteratif-20641)

## <br/> Observations :

### <br/> __Première observation__

L'algorithme mis en place est reconnu optimal pour le problème des Tours de Hanoï avec trois tours.

J'ignore s’il existe un algorithme aussi performant pour la variante des Tours de Hanoï avec 4 tours (voir plus).

Par conséquent, il est vivement conseillé de tester l'algorithme selon les régles du problème de base (avec trois tours donc).

<br/>
Des tests ont été effectués avec 4 tours et les résultats ne sont pas si satisfaisants.

Par exemple, avec 4 tours et 4 disques, le nombre de déplacements nécessaire pour résoudre le problème est supérieur à 1000.

### <br/> __Seconde observation :__

Afin de garantir un fonctionnement optimal du programme, il est vivement conseillé d’utiliser un nombre de disques toujours supérieur ou égale au nombre de tour.
En effet, si le nombre de disque est inférieur au nombre de tour, le programme ne fonctionne pas correctement.

## <br/> Description de l'affichage console :

<br/> 
Une fois le programme exécuté, la console affiche deux choses :

### <br/> __L'historique de jeu :__

<br/> 
L'historique de jeu permet d'avoir un aperçu du jeu dans sa totalité, de l'étape initial à l'étape finale.

Si le jeu se termine correctement, l'affichage considérera l'état final tel l'état du jeu terminé.

Si le jeu ne peut pas se terminer (avec un nombre d'étapes maximum trop faible par exemple),
Alors l'état final de l'historique correspondra au nombre d'étape maximum.

Dans cette historique, chaque étape est associée ici à plusieurs ensembles (un ensemble -> Une tour).
Par exemple, pour les tours de Hanoï avec 3 disques et 3 tours, à l'état initial, les ensembles sont :
- 1..3
- {}
- {}

Ces ensembles représentent donc les différentes tours, et bien entendu :
Dans chaque ensemble, nous retrouvons les disques placés sur la tour correspondante.

- Si un ensemble est composé de deux chiffres séparés par deux points successifs, cela signifie qu'il s'agit d'une simple suite (avec une incrémentation de 1) entre les deux chiffres.
    - Par exemple, l'ensemble 1..3 représente les disques 1, 2 et 3.

- Si l'ensemble est composé de deux chiffres identiques séparés par deux points successifs, cela signifie que cette tour ne possède qu'un seul disque : identifié par le chiffre en question.
    - Par exemple, l'ensemble 1..1 représente le disque 1.

- Et enfin, si un ensemble est composé de plusieurs chiffres séparés par des virgules, cela signifie que la tour possède les disques concernés.
    - Par exemple, l'ensemble {1, 4} représente les disques 1 et 4.

### <br/> __Le message de fin de jeu :__

Ce message permet simplement d'avertir l'utilisateur si le jeu s'est terminé ou non.

## <br/> Quelques captures d'écrans (affichage console)

### <br/> Résolution du problème pour 3 tours et 3 disques (nombre de déplacements limité à 7)
<br/> ![3 tours, 3 disques](images/win_3.png)

### <br/> Résolution du problème pour 3 tours et 4 disques (nombre de déplacements limité à 15)
<br/> ![3 tours, 4 disques](images/win_4.png)

### <br/> Résolution du problème pour 3 tours et 5 disques (nombre de déplacements limité à 31)
<br/> ![3 tours, 3 disques](images/win_5.png)

### <br/> Résolution du problème pour 3 tours et 6 disques (nombre de déplacements limité à 63)
<br/> ![3 tours, 4 disques](images/win_6.png)